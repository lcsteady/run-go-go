package com.run.gogo;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "rungogo")
public class RunGoGoProperties {

    private String confType = "xml";

    private String confPath;

    private List<String> confPaths;

    public String getConfType() {
        return confType;
    }

    public void setConfType(String confType) {
        this.confType = confType;
    }

    public String getConfPath() {
        return confPath;
    }

    public void setConfPath(String confPath) {
        this.confPath = confPath;
    }

    public List<String> getConfPaths() {
        return confPaths;
    }

    public void setConfPaths(List<String> confPaths) {
        this.confPaths = confPaths;
    }
}
