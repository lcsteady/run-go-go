package com.run.gogo;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@EnableConfigurationProperties(RunGoGoProperties.class)
@Import({RunGoGoSpringContext.class, RunGoGoConfig.class})
public @interface EenableRunGoGo {



}
