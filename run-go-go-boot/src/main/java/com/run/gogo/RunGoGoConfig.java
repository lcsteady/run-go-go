package com.run.gogo;

import com.run.gogo.context.ServiceContext;
import com.run.gogo.context.xml.XmlClasspathServiceFactory;
import com.run.gogo.util.Convert;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;

import java.io.FileNotFoundException;

public class RunGoGoConfig {

    @Autowired
    private RunGoGoProperties properties;

    @Autowired
    private RunGoGoSpringContext context;

    @Bean
    @Qualifier("rungogoServiceContext")
    public ServiceContext getServiceContext() throws FileNotFoundException {
        ServiceContext serviceContext = null;

        if(StringUtils.isEmpty(properties.getConfPath())
                && (null == properties.getConfPaths() || properties.getConfPaths().isEmpty()) ){
            throw new FileNotFoundException("run-go-go 配置文件不存在");
        }

        if(StringUtils.isNotEmpty(properties.getConfPath())){
            serviceContext = new XmlClasspathServiceFactory(context.getApplicationContext(),properties.getConfPath());
        }else{
            String[] paths = new String[properties.getConfPaths().size()];
            properties.getConfPaths().toArray(paths);
            serviceContext = new XmlClasspathServiceFactory(context.getApplicationContext(),paths);
        }
        return serviceContext;
    }

}
