package com.run.gogo.servicenode;

import com.run.gogo.action.AsycNodeListener;
import com.run.gogo.context.NodeContext;
import org.springframework.stereotype.Service;

@Service("listenertest")
public class ListenerTest implements AsycNodeListener {
    @Override
    public void complete(NodeContext nodeContext) {
        System.out.println("complete");
    }

    @Override
    public void fail(NodeContext nodeContext, Throwable ex) {
        System.out.println("fail");
    }
}
