package com.run.gogo.servicenode;

import com.run.gogo.action.NodeAction;
import com.run.gogo.context.NodeContext;
import org.springframework.stereotype.Service;

@Service("testServiceNodeAction")
public class TestServiceNodeAction implements NodeAction {

    @Override
    public void action(NodeContext nodeContext) {
        System.out.println("执行了");
    }

}
