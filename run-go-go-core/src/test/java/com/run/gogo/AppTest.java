package com.run.gogo;

import com.run.gogo.context.NodeMessage;
import com.run.gogo.context.ServiceContext;
import com.run.gogo.context.xml.XmlClasspathServiceFactory;
import com.run.gogo.servicenode.TestServiceNodeAction;
import com.run.gogo.spring.SpringBeanUtils;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppTest {

    @Test
    public void test1(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application.xml");

        ServiceContext serviceContext = new XmlClasspathServiceFactory(applicationContext,"services.xml");
        NodeMessage<String,Object> nodeContext = new NodeMessage();
        nodeContext.put("flag","1");
        nodeContext.put("flagcaseone","1");
        nodeContext.put("flagcasetwo","1");
        serviceContext.action("test",nodeContext);
    }
}
