package com.run.gogo.parse.xml;

import com.run.gogo.constant.Constant;
import com.run.gogo.exception.NodeException;
import com.run.gogo.parse.NodeParse;

public class XmlParseNodeStrategy {

    private static XmlParseNodeStrategy xmlParseNode = null;

    private XmlParseNodeStrategy(){}


    public static XmlParseNodeStrategy getInstance(){
        if(null == xmlParseNode){
            xmlParseNode = new XmlParseNodeStrategy();
        }
        return xmlParseNode;
    }

    public NodeParse getNodeParse(String xmlTag){

        switch (xmlTag){
            case Constant.SERVICE_TAG:
                return XmlServiceParse.getInstance();
            case Constant.SERVICE_NODE_TAG:
                return XmlServiceNodeParse.getInstance();
            case Constant.CONDITION_TAG:
                return XmlConditionNodeParse.getInstance();
            case Constant.SWITCH_TAG:
                return XmlSwitchNodeParse.getInstance();
            case Constant.CASE_TAG:
                return XmlCaseNodeParse.getInstance();
            case Constant.ASYC_TAG:
                return XmlAsycNodeParse.getInstance();
            case Constant.TRANSACTION_TAG:
                return XmlTransactionNodeParse.getInstance();
            case Constant.REF_SERVICE_TAG:
                return XmlRefServiceNodeParse.getInstance();
        }

        throw new NodeException(xmlTag + " no node");
    }

}
