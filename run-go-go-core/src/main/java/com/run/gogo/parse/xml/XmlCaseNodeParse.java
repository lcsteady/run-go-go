package com.run.gogo.parse.xml;

import com.run.gogo.constant.Constant;
import com.run.gogo.node.CaseNode;
import com.run.gogo.node.Node;
import com.run.gogo.util.Convert;
import org.dom4j.Element;

/**
 * 解析多分支判断标签
 * */
public class XmlCaseNodeParse extends AbstractXmlNodeParse {


    private static XmlCaseNodeParse nodeParse = null;

    private XmlCaseNodeParse(){}


    public static XmlCaseNodeParse getInstance(){
        if(null == nodeParse){
            nodeParse = new XmlCaseNodeParse();
        }
        return nodeParse;
    }

    @Override
    public Node parseToNode(Element element) {
        String name = element.attributeValue(Constant.NAME_ATT);
        String desc = element.attributeValue(Constant.DESC_ATT);
        String cond = element.attributeValue(Constant.COND_ATT);
        String variable = element.attributeValue(Constant.VARIABLE_ATT);
        String isDefaultStr = element.attributeValue(Constant.ISDEFAULT_ATT);
        String isBreakStr = element.attributeValue(Constant.ISBREAK_ATT);
        Boolean isDefault = Convert.toBool(isDefaultStr,false);
        Boolean isBreak = Convert.toBool(isBreakStr,false);
        Node innerNode = innerNodeParse(element);
        CaseNode caseNode = new CaseNode(name,desc,cond,variable,innerNode,isDefault,isBreak);
        return caseNode;
    }

}
