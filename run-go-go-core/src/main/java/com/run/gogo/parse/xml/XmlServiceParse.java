package com.run.gogo.parse.xml;

import com.run.gogo.constant.Constant;
import com.run.gogo.node.Node;
import com.run.gogo.node.Service;
import org.dom4j.Element;


/**
 * 解析服务标签
 * */
public class XmlServiceParse extends AbstractXmlNodeParse {


    private static XmlServiceParse nodeParse = null;

    private XmlServiceParse(){}


    public static XmlServiceParse getInstance(){
        if(null == nodeParse){
            nodeParse = new XmlServiceParse();
        }
        return nodeParse;
    }

    @Override
    public Node parseToNode(Element element) {
        String name = element.attributeValue(Constant.NAME_ATT);
        String desc = element.attributeValue(Constant.DESC_ATT);
        Node innerNode = innerNodeParse(element);
        Node node = new Service(name,desc,innerNode);
        return node;
    }


}
