package com.run.gogo.parse.xml;

import com.run.gogo.constant.Constant;
import com.run.gogo.node.AsycNode;
import com.run.gogo.node.Node;
import org.dom4j.Element;

/**
 * 解析异步标签
 * */
public class XmlAsycNodeParse extends AbstractXmlNodeParse {


    private static XmlAsycNodeParse nodeParse = null;

    private XmlAsycNodeParse(){}


    public static XmlAsycNodeParse getInstance(){
        if(null == nodeParse){
            nodeParse = new XmlAsycNodeParse();
        }
        return nodeParse;
    }

    @Override
    public Node parseToNode(Element element) {
        String name = element.attributeValue(Constant.NAME_ATT);
        String desc = element.attributeValue(Constant.DESC_ATT);
        String listenerClassName = element.attributeValue(Constant.LISTENER_CLASS_NAME_ATT);
        String listenerSpringBeanName = element.attributeValue(Constant.LISTENER_SPRING_BEAN_NAME_ATT);
        Node innerNode = innerNodeParse(element);
        AsycNode asycNode = new AsycNode(name,desc,listenerClassName,listenerSpringBeanName,innerNode);
        return asycNode;
    }

}
