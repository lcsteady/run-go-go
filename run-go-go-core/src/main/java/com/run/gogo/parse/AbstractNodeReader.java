package com.run.gogo.parse;

import com.run.gogo.factory.ServiceRegistry;
import com.run.gogo.io.Resource;

/**
 * 节点读取抽象实现
 * */
public abstract class AbstractNodeReader implements NodeReader {

    private ServiceRegistry serviceRegistry;

    public AbstractNodeReader(ServiceRegistry serviceRegistry){
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    @Override
    public void loadNode(Resource... resources) {
        for (Resource resource:resources){
            loadNode(resource);
        }
    }
}
