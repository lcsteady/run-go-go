package com.run.gogo.parse.xml;

import com.run.gogo.constant.Constant;
import com.run.gogo.node.Node;
import com.run.gogo.node.RefServiceNode;
import com.run.gogo.node.SwitchNode;
import org.dom4j.Element;


/**
 * 解析多分支判断标签
 * */
public class XmlRefServiceNodeParse extends AbstractXmlNodeParse {


    private static XmlRefServiceNodeParse nodeParse = null;

    private XmlRefServiceNodeParse(){}


    public static XmlRefServiceNodeParse getInstance(){
        if(null == nodeParse){
            nodeParse = new XmlRefServiceNodeParse();
        }
        return nodeParse;
    }

    @Override
    public Node parseToNode(Element element) {

        String name = element.attributeValue(Constant.NAME_ATT);
        String desc = element.attributeValue(Constant.DESC_ATT);
        String refName = element.attributeValue(Constant.REF_NAME_ATT);
        RefServiceNode switchNode = new RefServiceNode(name,desc,refName);
        return switchNode;
    }
}
