package com.run.gogo.parse.xml;

import com.run.gogo.constant.Constant;
import com.run.gogo.node.CaseNode;
import com.run.gogo.node.Node;
import com.run.gogo.node.SwitchNode;
import com.run.gogo.parse.NodeParse;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.List;


/**
 * 解析多分支判断标签
 * */
public class XmlSwitchNodeParse extends AbstractXmlNodeParse {


    private static XmlSwitchNodeParse nodeParse = null;

    private XmlSwitchNodeParse(){}


    public static XmlSwitchNodeParse getInstance(){
        if(null == nodeParse){
            nodeParse = new XmlSwitchNodeParse();
        }
        return nodeParse;
    }

    @Override
    public Node parseToNode(Element element) {

        String name = element.attributeValue(Constant.NAME_ATT);
        String desc = element.attributeValue(Constant.DESC_ATT);
        Node innerNode = innerNodeParse(element);
        SwitchNode switchNode = new SwitchNode(name,desc,innerNode);
        return switchNode;
    }
}
