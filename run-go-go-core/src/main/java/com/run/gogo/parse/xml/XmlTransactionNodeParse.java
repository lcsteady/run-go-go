package com.run.gogo.parse.xml;

import com.run.gogo.constant.Constant;
import com.run.gogo.node.Node;
import com.run.gogo.node.TransactionNode;
import com.run.gogo.util.Convert;
import org.dom4j.Element;

/**
 * 解析事务标签
 * */
public class XmlTransactionNodeParse extends AbstractXmlNodeParse{


    private static XmlTransactionNodeParse nodeParse = null;

    private XmlTransactionNodeParse(){}


    public static XmlTransactionNodeParse getInstance(){
        if(null == nodeParse){
            nodeParse = new XmlTransactionNodeParse();
        }
        return nodeParse;
    }

    @Override
    public Node parseToNode(Element element) {
        String name = element.attributeValue(Constant.NAME_ATT);
        String desc = element.attributeValue(Constant.DESC_ATT);
        String isolation = element.attributeValue(Constant.ISOLATION_ATT);
        Boolean readOnly = Convert.toBool(element.attributeValue(Constant.READONLY_ATT),false);
        String propagation = element.attributeValue(Constant.PROPAGATION_ATT);
        String transactionManager = element.attributeValue(Constant.TRANSACTIONMANAGER_ATT);
        String rollbackFor = element.attributeValue(Constant.ROLLBACKFOR_ATT);
        String noRollbackFor = element.attributeValue(Constant.NOROLLBACKFOR_ATT);
        Integer timeout = Convert.toInt(element.attributeValue(Constant.TIMEOUT_ATT));
        Node innerNode = innerNodeParse(element);
        TransactionNode transactionNode = new TransactionNode(name,desc,isolation,readOnly,propagation,transactionManager
                ,rollbackFor,noRollbackFor,timeout,innerNode);
        return transactionNode;
    }
}
