package com.run.gogo.parse.xml;

import com.run.gogo.constant.Constant;
import com.run.gogo.node.ConditionNode;
import com.run.gogo.node.Node;
import org.dom4j.Element;

/**
 * 解析条件判断标签
 * */
public class XmlConditionNodeParse extends AbstractXmlNodeParse {


    private static XmlConditionNodeParse nodeParse = null;

    private XmlConditionNodeParse(){}


    public static XmlConditionNodeParse getInstance(){
        if(null == nodeParse){
            nodeParse = new XmlConditionNodeParse();
        }
        return nodeParse;
    }

    @Override
    public Node parseToNode(Element element) {

        String name = element.attributeValue(Constant.NAME_ATT);
        String desc = element.attributeValue(Constant.DESC_ATT);
        String cond = element.attributeValue(Constant.COND_ATT);
        String variable = element.attributeValue(Constant.VARIABLE_ATT);
        Node innerNode = innerNodeParse(element);
        ConditionNode conditionNode = new ConditionNode(name,desc,cond,variable,innerNode);
        return conditionNode;
    }
}
