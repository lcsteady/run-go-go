package com.run.gogo.context.xml;

import com.run.gogo.context.AbstractServiceContext;
import com.run.gogo.factory.DefaultListableServiceFactory;
import com.run.gogo.factory.ServiceFactory;
import com.run.gogo.io.ClassPathXmlResoutce;
import com.run.gogo.io.Resource;
import com.run.gogo.parse.xml.XmlClassPathNodeReader;
import com.run.gogo.spring.SpringBeanUtils;
import org.springframework.context.ApplicationContext;


public class XmlClasspathServiceFactory extends AbstractServiceContext {

    private String [] classPaths;
    private XmlClassPathNodeReader reader;

    public XmlClasspathServiceFactory(ApplicationContext applicationContext,String ... classPaths){
        super(applicationContext);
        this.classPaths = classPaths;
        init();

    }

    public XmlClasspathServiceFactory(String ... classPaths){
        this.classPaths = classPaths;
        init();
    }

    private DefaultListableServiceFactory serviceFactory;

    @Override
    public ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    private void init() {
        synchronized (this){
            if (null == serviceFactory){
                serviceFactory = new DefaultListableServiceFactory();
                reader = new XmlClassPathNodeReader(serviceFactory);
            }
        }
        reader.loadNode(getResources());
    }


    /**
     * 获取所有资源
     * */
    public Resource [] getResources(){
        Resource [] resources = new Resource[classPaths.length];
        for (int i=0;i<classPaths.length;i++){
           resources[i] = new ClassPathXmlResoutce(classPaths[i]);
        }
        return resources;
    }
}
