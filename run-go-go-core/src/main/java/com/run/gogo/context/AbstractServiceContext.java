package com.run.gogo.context;

import com.run.gogo.factory.ServiceFactory;
import com.run.gogo.node.Node;
import com.run.gogo.spring.SpringBeanUtils;
import org.springframework.context.ApplicationContext;

public abstract class AbstractServiceContext implements ServiceContext {


    public AbstractServiceContext(){

    }

    public AbstractServiceContext(ApplicationContext applicationContext){
        SpringBeanUtils.getInstance().setApplicationContext(applicationContext);
    }

    @Override
    public Node getNode(String name) {
        return getServiceFactory().getNode(name);
    }

    /**
     * 根据名称输入参数执行节点
     * */
    @Override
    public NodeMessage action(String name,NodeMessage<String,Object> nodeMessage){
        NodeContext nodeContext = new NodeContext(nodeMessage);
        this.getNode(name).action(nodeContext);
        return nodeContext.getOut();
    }

    public abstract ServiceFactory getServiceFactory();

}
