package com.run.gogo.factory;

import com.run.gogo.node.Node;

/**
 * 服务工厂
 * 所有的额流程服务都在这个里面获取
 * */
public interface ServiceFactory {

    /**
     * 根据名称获取
     * */
    Node getNode(String name);
}
