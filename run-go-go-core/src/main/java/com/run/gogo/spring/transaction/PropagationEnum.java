package com.run.gogo.spring.transaction;

public enum PropagationEnum {

    REQUIRED("REQUIRED",0),
    SUPPORTS("SUPPORTS",1),
    MANDATORY("MANDATORY",2),
    REPEATABLE_READ("REPEATABLE_READ",4),
    REQUIRES_NEW("REQUIRES_NEW",4),
    NOT_SUPPORTED("NOT_SUPPORTED",4),
    NEVER("NEVER",4),
    NESTED("NESTED",4);
    private String name;
    private int value;

    PropagationEnum(String name, int value){
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static int getValue(String name){
        for(PropagationEnum propagation: PropagationEnum.values()){
            if(propagation.getName().equals(name)){
                return propagation.getValue();
            }
        }
        return 0;
    }

}
