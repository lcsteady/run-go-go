package com.run.gogo.spring.transaction;

public enum IsolationEnum {

    DEFAULT("DEFAULT",-1),
    READ_UNCOMMITTED("READ_UNCOMMITTED",1),
    READ_COMMITTED("READ_COMMITTED",2),
    REPEATABLE_READ("REPEATABLE_READ",4),
    SERIALIZABLE("SERIALIZABLE",8);

    private String name;
    private int value;

    IsolationEnum(String name,int value){
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static int getValue(String name){
        for(IsolationEnum isolationEnum:IsolationEnum.values()){
            if(isolationEnum.getName().equals(name)){
                return isolationEnum.getValue();
            }
        }
        return -1;
    }

}
