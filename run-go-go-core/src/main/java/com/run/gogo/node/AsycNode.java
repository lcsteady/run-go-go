package com.run.gogo.node;

import com.run.gogo.action.AsycNodeAction;


/**
 * 异步执行接口
 * */
public class AsycNode extends AbstractNode {

    /**
     * 监听类
     * */
    private String listenerClassName;

    private String listenerSpringBeanName;

    public AsycNode(){

    }

    public AsycNode(String name,String desc,String listenerClassName,String listenerSpringBeanName,Node innerNode){
        super(name,desc);
        this.innerNode = innerNode;
        nodeAction = new AsycNodeAction(this.name,listenerClassName,listenerSpringBeanName,innerNode);
    }

}
