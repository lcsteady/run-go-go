package com.run.gogo.node;

import com.run.gogo.action.TransactionNodeAction;
import com.run.gogo.spring.transaction.IsolationEnum;
import com.run.gogo.spring.transaction.PropagationEnum;
import com.run.gogo.util.Convert;

public class TransactionNode extends AbstractNode {

    /* 隔离级别 */
    private String isolation;

    /* 只读 */
    private Boolean readOnly;

    /* 传播属性 */
    private String propagation;

    /* spring事务管理器 名称 */
    private String transactionManager;

    /* 回滚异常 */
    private String rollbackFor;

    /* 不回滚异常 */
    private String noRollbackFor;

    /* 事务超时时间 */
    private Integer timeout;

    public TransactionNode(){

    }

    public TransactionNode(String name,String desc,String isolation,Boolean readOnly,String propagation,String transactionManager
            ,String rollbackFor,String noRollbackFor,Integer timeout,Node innerNode){
        super(name,desc);
        this.isolation = isolation;
        this.readOnly = readOnly;
        this.propagation = propagation;
        this.transactionManager = transactionManager;
        this.rollbackFor = rollbackFor;
        this.noRollbackFor = noRollbackFor;
        this.innerNode = innerNode;
        this.timeout = timeout;
        nodeAction = new TransactionNodeAction(this.isolation,this.readOnly,this.propagation,this.transactionManager,this.rollbackFor,this.noRollbackFor,this.timeout,this.innerNode);
    }

}
