package com.run.gogo.node;

import com.run.gogo.action.CaseNodeAction;
import com.run.gogo.context.NodeContext;
import com.run.gogo.util.Assert;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CaseNode extends ConditionNode {

    private static final Log log = LogFactory.getLog(CaseNode.class);

    /**
     * 是否是默认
     * */
    private Boolean isDefault;

    /**
     * 是否 直接返回
     * */
    private Boolean isBreak;

    /**
     * case 执行 action
     */
    private CaseNodeAction nodeAction;



    public CaseNode(){
        super();
    }

    public CaseNode(String name,String desc,String cond,String variable, Node innerNode,Boolean isDefault,Boolean isBreak) {
        super();
        this.name = name;
        this.desc = desc;
        this.innerNode = innerNode;
        this.variable = variable;
        this.isBreak = isBreak;
        this.isDefault = isDefault;
        this.cond = cond;
        if(!isDefault){
            Assert.isNotEmpty(cond,"Case node cond not null");
        }
        this.nodeAction = new CaseNodeAction(this.cond,this.variable,this.isDefault,this.innerNode);
    }

    @Override
    public void action(NodeContext nodeContext) {
        log.info(String.format("执行-%s",this.name));
        Boolean flag =  nodeAction.actionExt(nodeContext);
        if(flag && isBreak){
            return;
        }
        nextNodeAction(nodeContext);
    }

    public Boolean isDefault() {
        return isDefault;
    }

    public Boolean isBreak() {
        return isBreak;
    }

}
