package com.run.gogo.node;

import com.run.gogo.action.ConditionNodeAction;
import com.run.gogo.util.Assert;

/**
 * 添加判断节点
 * */
public class ConditionNode extends AbstractNode {

    protected String cond;

    protected String variable;

    public ConditionNode(){
        super();
    }

    public ConditionNode(String name,String desc,String cond,String variable,Node innerNode){
        super(name,desc);
        Assert.isNotEmpty(cond,"Condition node cond not null");
        this.cond = cond;
        this.variable = variable;
        this.innerNode = innerNode;
        this.nodeAction = new ConditionNodeAction(this.cond,this.variable,this.innerNode);
    }



}
