package com.run.gogo.node;

import com.run.gogo.action.ServiceAction;
import com.run.gogo.context.NodeContext;

/**
 * 定义服务
 * */
public class Service extends AbstractNode {

    public Service(String name,String desc,Node innerNode){
        super(name,desc);
        this.innerNode = innerNode;
        this.nodeAction = new ServiceAction(this.name,this.innerNode);
    }

    @Override
    public void action(NodeContext nodeContext) {
        /**
         * 执行当前节点
         * */
        nodeAction.action(nodeContext);
    }
}
