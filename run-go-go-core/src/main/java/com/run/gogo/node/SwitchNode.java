package com.run.gogo.node;

import com.run.gogo.action.SwitchNodeAction;

import java.util.List;

/**
 * 多分支判断
 * */
public class SwitchNode extends AbstractNode{


    public SwitchNode(){
        super();
    }

    public SwitchNode(String name,String desc,Node innerNode){
        super(name,desc);
        this.innerNode = innerNode;
        this.nodeAction = new SwitchNodeAction(this.innerNode);
    }

}
