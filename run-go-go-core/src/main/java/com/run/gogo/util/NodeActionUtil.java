package com.run.gogo.util;

import com.run.gogo.action.AsycNodeListener;
import com.run.gogo.action.NodeAction;
import com.run.gogo.exception.NodeException;
import com.run.gogo.spring.SpringBeanUtils;

/**
 * 获取nodeAction执行类
 * */
public class NodeActionUtil {


    public static NodeAction getNodeActionByClass(String className){
        NodeAction action = null;
        try {
            Class c = Class.forName(className);
            action = (NodeAction) c.newInstance();
        } catch (ClassNotFoundException e) {
            throw new NodeException("service node clessname create object fail", e);
        } catch (IllegalAccessException e) {
            throw new NodeException("service node clessname create object fail", e);
        } catch (InstantiationException e) {
            throw new NodeException("service node clessname create object fail", e);
        }
        return action;
    }

    public static NodeAction getNodeActionBySpring(String springBeanName){
        NodeAction nodeAction = (NodeAction) SpringBeanUtils.getInstance().getBean(springBeanName);
        Assert.isNotNull(nodeAction,"service node spring-bean-name get null");
        return nodeAction;
    }


    public static AsycNodeListener getAsycNodeListenerByClass(String className){
        AsycNodeListener action = null;
        try {
            Class c = Class.forName(className);
            action = (AsycNodeListener) c.newInstance();
        } catch (ClassNotFoundException e) {
            throw new NodeException("service node clessname create object fail", e);
        } catch (IllegalAccessException e) {
            throw new NodeException("service node clessname create object fail", e);
        } catch (InstantiationException e) {
            throw new NodeException("service node clessname create object fail", e);
        }
        return action;
    }

    public static AsycNodeListener getAsycNodeListenerBySpring(String springBeanName){
        AsycNodeListener nodeAction = (AsycNodeListener) SpringBeanUtils.getInstance().getBean(springBeanName);
        Assert.isNotNull(nodeAction,"service node spring-bean-name get null");
        return nodeAction;
    }

}
