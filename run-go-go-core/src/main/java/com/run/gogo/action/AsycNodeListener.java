package com.run.gogo.action;

import com.run.gogo.context.NodeContext;

/**
 * 异步节点 回调接口
 * */
public interface AsycNodeListener {

    /**
     * 成功回调
     * */
    void complete(NodeContext nodeContext);

    /**
     * 失败回调
     * */
    void fail(NodeContext nodeContext, Throwable ex);

}
