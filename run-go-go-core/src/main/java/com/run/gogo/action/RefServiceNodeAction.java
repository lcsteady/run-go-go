package com.run.gogo.action;

import com.run.gogo.context.NodeContext;
import com.run.gogo.node.Node;

/**
 * 定义服务节点 action不需要有内容
 * */
public class RefServiceNodeAction implements NodeAction {
    private Node innerNode;

    public RefServiceNodeAction(Node innerNode){
        this.innerNode = innerNode;
    }

    @Override
    public void action(NodeContext nodeContext) {
        innerNode.action(nodeContext);
    }
}
