package com.run.gogo.action;

import com.run.gogo.context.NodeContext;
import com.run.gogo.node.Node;
import com.run.gogo.util.RuelUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *  逻辑处理 juel
 * */
public class ConditionNodeAction implements NodeAction {
    private static final Log log = LogFactory.getLog(ConditionNodeAction.class);

    protected String cond;

    protected String variable;

    protected Node innerNode;

    public ConditionNodeAction(String cond,String variable,Node innerNode){
        this.cond = cond;
        this.variable = variable;
        this.innerNode = innerNode;
    }

    @Override
    public void action(NodeContext nodeContext) {
        String [] variables = null;
        if(StringUtils.isNotEmpty(variable)){
            variables = variable.split(",");
        }

        Boolean flag = RuelUtil.execute(cond,nodeContext.getIn(),variables);
        log.info(String.format("逻辑处理结果:%s",flag));
        if(flag){
            innerNode.action(nodeContext);
        }
    }
}
