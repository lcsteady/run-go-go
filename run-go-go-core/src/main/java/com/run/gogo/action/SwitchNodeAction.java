package com.run.gogo.action;

import com.run.gogo.context.NodeContext;
import com.run.gogo.exception.NodeException;
import com.run.gogo.node.CaseNode;
import com.run.gogo.node.Node;

import java.util.List;

/**
 * 多分支判断执行
 * */
public class SwitchNodeAction implements NodeAction {

    private Node innerNode;


    public SwitchNodeAction(Node innerNode){
        this.innerNode = innerNode;
    }

    @Override
    public void action(NodeContext nodeContext) {
        if(null == innerNode){
            return;
        }
        innerNode.action(nodeContext);
    }
}
