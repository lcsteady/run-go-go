package com.run.gogo.action;

import com.run.gogo.context.NodeContext;
import com.run.gogo.node.Node;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 定义服务节点 action不需要有内容
 * */
public class ServiceAction implements NodeAction {
    private static final Log log = LogFactory.getLog(ServiceAction.class);
    private Node innerNode;

    private String name;

    public ServiceAction(String name,Node innerNode){
        this.name = name;
        this.innerNode = innerNode;
    }

    @Override
    public void action(NodeContext nodeContext) {
        log.info(String.format("%s - 服务流程开始", name));
        innerNode.action(nodeContext);
        log.info(String.format("%s - 服务流程结束", name));
    }
}
