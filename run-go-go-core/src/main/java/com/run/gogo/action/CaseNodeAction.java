package com.run.gogo.action;

import com.run.gogo.context.NodeContext;
import com.run.gogo.node.ConditionNode;
import com.run.gogo.node.Node;
import com.run.gogo.util.RuelUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *  逻辑处理 juel
 * */
public class CaseNodeAction extends ConditionNodeAction {

    private static final Log log = LogFactory.getLog(ConditionNodeAction.class);

    private Boolean isDefault;

    public CaseNodeAction(String cond,String variable,Boolean isDefault, Node innerNode){
        super(cond,variable,innerNode);
        this.isDefault = isDefault;
    }

    @Override
    public void action(NodeContext nodeContext) {
        innerNode.action(nodeContext);
    }

    public Boolean actionExt(NodeContext nodeContext){
        if(isDefault){
            this.action(nodeContext);
            return true;
        }else{
            String [] variables = null;
            if(StringUtils.isNotEmpty(variable)){
                variables = variable.split(",");
            }
            Boolean flag = RuelUtil.execute(cond,nodeContext.getIn(),variables);
            log.info(String.format("逻辑处理结果:%s",flag));
            if(flag){
                this.action(nodeContext);
            }
            return flag;
        }
    }
}
