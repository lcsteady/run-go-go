package com.run.gogo.action;

import com.run.gogo.context.NodeContext;

/**
 * 节点执行
 * */
public interface NodeAction {


    void action(NodeContext nodeContext);

}
