# run-go-go

#### 介绍
    run-go-go，是一个流程编排框架，把一个接口中的每一步都抽象成一个节点来完成一个接口的业务处理，
    主要是为了降低代码之间的耦合度，业务流程更清晰，二次开发更简单。

#### 软件架构
    spring-tx:5.1.8
    spring-context:5.1.8
    dom4j:1.6.1
    juel:2.2.7
    log4j:2.12.0



#### 安装教程
    1.下载源码 mvn deploy
    2.maven 引入

    <dependency>
       <groupId>com.run.gogo</groupId>
       <artifactId>run-go-go</artifactId>
       <version>1.0</version>
    </dependency>


#### 使用说明

    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application.xml");
    ServiceContext serviceContext = new XmlClasspathServiceFactory(applicationContext,"services.xml");
    NodeMessage<String,Object> nodeContext = new NodeMessage();
    nodeContext.put("key","value");
    serviceContext.action("test",nodeContext);
    
    wiki地址：https://gitee.com/lcsteady/run-go-go/wikis/pages
    
#### 配置样例
    <?xml version="1.0" encoding="UTF-8" ?>
    <services>
    
        <service name="test" desc="测试流程">
            <service-node name="测试节点1" desc="测试节点1" class-name="com.run.gogo.servicenode.TestServiceNodeAction" />
            <asyc name="异步处理" desc="一般通知会用到">
                <service-node name="测试节点4" desc="测试节点4" class-name="com.run.gogo.servicenode.TestServiceNodeAction" />
            </asyc>
            <condition name="condition节点" cond="${flag == '2'}" desc="condition节点">
                <service-node name="测试节点5" desc="测试节点5" class-name="com.run.gogo.servicenode.CondServiceNodeAction" />
            </condition>
            <switch name="switch" desc="switch">
                <case cond="${flagcaseone == '1'}" is-break="true"  name="第一个判断">
                    <service-node name="测试节点2" desc="测试节点2" class-name="com.run.gogo.servicenode.TestServiceNodeAction" />
                </case>
                <case is-default="true" name="第二个判断">
                    <service-node name="测试节点3" desc="测试节点3" class-name="com.run.gogo.servicenode.TestServiceNodeAction" />
                </case>
            </switch>
        </service>
    
    </services>

#### 参与贡献

    淡_定：lcsteady@163.com 吐槽和建议邮箱 欢迎欢迎
    
